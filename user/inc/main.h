/**
 * @file		main.h
 * @author		Chugay E.A.
 * @version		0.01
 * @date		22.02.2022
 * @brief       Базовый проект для микроконтроллера STM32F031F6P6.
 */

#ifndef MAIN_H_
#define MAIN_H_

/*----------------------------------------------------------------- Includes: */
#include "stm32l0xx.h"
#include "button.h"
#include "ssd1306.h"
#include "screen.h"
#include "state_machine.h"

/*-------------------------------------------------------------------- Macro: */
//  Сигналы управления:
enum Signal {
    SIGNAL_NONE = 0,
    SIGNAL_TIMEOUT,
    SIGNAL_BTN_L_CLICK,
    SIGNAL_BTN_U_CLICK,
    SIGNAL_BTN_D_CLICK,
    SIGNAL_BTN_R_CLICK,
    SIGNAL_BTN_L_LONG,
    SIGNAL_BTN_U_LONG,
    SIGNAL_BTN_D_LONG,
    SIGNAL_BTN_R_LONG
};

typedef union {
    struct {
        uint32_t display_cnt    :8;
        uint32_t btn_cnt        :4;
        uint32_t btn_upd        :1;
        uint32_t reserve        :19;
    };
    uint32_t value;
} FLAG_T;

typedef union {
    struct {
        uint32_t ms;
        uint32_t sec;
    };
    uint32_t value;
} UPTIME_T;

/*------------------------------------------------------- Function prototype: */

#endif /* MAIN_H_ */
