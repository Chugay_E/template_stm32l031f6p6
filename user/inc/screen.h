/**
 * @file		screen.h
 * @author		Chugay E.A.
 * @version		0.01
 * @date		22.04.2020
 * @brief		Функции отрисовки экрана с использованием графических примитивов
 * File used UTF-8 code page. Don't convert other codepage for correct work.
 */

#ifndef SCREEN_H_
#define SCREEN_H_

/*----------------------------------------------------------------- Includes: */
#include "display_config.h"
#include "frame_builder.h"
#include "widget.h"
#include "main.h"
#include "state_machine.h"

/*-------------------------------------------------------------------- Macro: */
enum MessageID {
    MSG_ERROR = 0,
    MSG_WARNING
};

/*------------------------------------------------------ Function prototypes: */
void top_level_screen (uint8_t signal);
void msg_screen (uint8_t msg_id);

#endif /* SCREEN_H_ */
