/**
 * @file		state_machine.h
 * @author		Chugay E.A.
 * @version		0.01
 * @date		22.04.2020
 * @brief		Функции автоматов состояния
 */

#ifndef STATE_MACHINE_H_
#define STATE_MACHINE_H_

/*----------------------------------------------------------------- Includes: */
#include <stdint.h>

/*-------------------------------------------------------------------- Macro: */
enum TopLevelState {
    STR_1 = 0,
    STR_2,
    STR_3,
    STR_4,
    STR_5,
    STR_6,
    STR_7,
    STR_8,
    TOP_LEVEL_SELECT
};

/*------------------------------------------------------ Function prototypes: */
void top_level_sm (uint8_t signal);


#endif /* STATE_MACHINE_H_ */
