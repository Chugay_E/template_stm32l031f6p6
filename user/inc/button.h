/**
 * @file		button.h
 * @author		Chugay E.A.
 * @version		0.02
 * @date		03.04.2021
 * @brief		Функции инициализации входов GPIO и обработки нажатия кнопок.
 */

#ifndef BUTTON_H_
#define BUTTON_H_

/*----------------------------------------------------------------- Includes: */
#include "stm32l0xx.h"

/*-------------------------------------------------------------------- Macro: */
#define BTN_L	(GPIOA->IDR & GPIO_IDR_ID7)
#define BTN_U	(GPIOA->IDR & GPIO_IDR_ID6)
#define BTN_D	(GPIOA->IDR & GPIO_IDR_ID5)
#define BTN_R   (GPIOA->IDR & GPIO_IDR_ID4)

typedef union {
    struct {
        volatile uint8_t l_click :1;
        volatile uint8_t u_click :1;
        volatile uint8_t d_click :1;
        volatile uint8_t r_click :1;
        volatile uint8_t l_long  :1;
        volatile uint8_t u_long  :1;
        volatile uint8_t d_long  :1;
        volatile uint8_t r_long  :1;
    };
    volatile uint8_t value;
} BTN_SIGNAL;

/*------------------------------------------------------- Function prototype: */
/**
 * @brief   Инициализация GPIO как входы кнопок.
 * @param   Нет.
 * @retval  Нет.
 */
void init_button_pin (void);

/**
 * @brief   Обработка нажитай кнопок, выдача
 * @param   Нет.
 * @retval  Сигнал о нажатии или удержании кнопки (см. структуру BTN_SIGNAL).
 */
uint8_t button_debounce (void);

#endif /* BUTTON_H_ */
