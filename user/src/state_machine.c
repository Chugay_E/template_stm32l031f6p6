/**
 * @file		state_machine.c
 * @author		Chugay E.A.
 * @version		0.01
 * @date		22.04.2020
 * @brief		Функции автоматов состояния
 *              *_sm - state machine
 *              *_cs - current state
 */

/*----------------------------------------------------------------- Includes: */
#include "state_machine.h"
#include "main.h"
#include "screen.h"

/*-------------------------------------------------------------------- Macro: */

/*--------------------------------------------------------- Extern variables: */
extern FLAG_T status_flag;

/*--------------------------------------------------------- Public variables: */

/*-------------------------------------------------------- Private variables: */
static enum TopLevelState top_level_cs = TOP_LEVEL_SELECT;

/*----------------------------------------------------------------- Function: */
void top_level_sm (uint8_t signal)
{
    static uint8_t tl_tmp = 0;

    switch (top_level_cs) {
    case TOP_LEVEL_SELECT:
        switch (signal) {
        case SIGNAL_BTN_U_CLICK: case SIGNAL_BTN_U_LONG:
            (tl_tmp > 0) ? (tl_tmp--) : (tl_tmp = (TOP_LEVEL_SELECT - 1));
            break;
        case SIGNAL_BTN_D_CLICK: case SIGNAL_BTN_D_LONG:
            (tl_tmp < (TOP_LEVEL_SELECT - 1)) ? (tl_tmp++) : (tl_tmp = 0);
            break;
        case SIGNAL_BTN_L_CLICK:
            tl_tmp = 0;
            break;
        case SIGNAL_BTN_R_CLICK:
            top_level_cs = tl_tmp;
            break;
        }
        if (display_upd_get()) send_frame_to_display(tl_tmp, top_level_screen);
        break;
    case STR_1:
    case STR_2:
    case STR_3:
    case STR_4:
    case STR_5:
    case STR_6:
    case STR_7:
    case STR_8:
        if (signal == SIGNAL_BTN_L_CLICK) {
            top_level_cs = TOP_LEVEL_SELECT;
            tl_tmp = 0;
        }
        if (display_upd_get()) send_frame_to_display(MSG_ERROR, msg_screen);
        break;
    }
}
