/**
 * @file		main.c
 * @author		Chugay E.A.
 * @version		0.01
 * @date		22.02.2022
 * @brief
 */

/*----------------------------------------------------------------- Includes: */
#include "main.h"

/*-------------------------------------------------------------------- Macro: */
#define TIMEOUT_VALUE       ((uint16_t)500)
#define IWDG_START          (uint32_t)(0x0000CCCC)
#define IWDG_WRITE_ACCESS   (uint32_t)(0x00005555)
#define IWDG_REFRESH        (uint32_t)(0x0000AAAA)      //	Reset watchdog
#define IWDG_RELOAD         (300)                       //  ~2 sec.
#define IWDG_RESET          (IWDG->KR = IWDG_REFRESH)

/*--------------------------------------------------------- Extern variables: */

/*--------------------------------------------------------- Public variables: */
volatile UPTIME_T  uptime;
volatile FLAG_T    flag;

/*-------------------------------------------------------- Private variables: */
static enum Signal current_signal = SIGNAL_NONE;
static BTN_SIGNAL  btn;

/*---------------------------------------------- Private funcrion prototypes: */
void RCC_init(void);
void IWDG_init(void);
void SysTick_init(void);
void test_pin_init(void);

/*----------------------------------------------------------------- Function: */
int main (void)
{
    RCC_init();
    IWDG_init();
    SysTick_init();
    init_display();
    init_button_pin();
    test_pin_init();

    __enable_irq();

    uptime.value = 0;
    flag.value   = 0;

    while (1)
    {
        //  Сброс сторожевого таймера:
        IWDG_RESET;

        //  опрос кнопок:
        if (flag.btn_upd) {
            flag.btn_upd = 0;
            btn.value = button_debounce();
            //  Формирование сигнала для автомата состояний:
            if (btn.l_click) {
                current_signal = SIGNAL_BTN_L_CLICK;
            } else if (btn.u_click) {
                current_signal = SIGNAL_BTN_U_CLICK;
            } else if (btn.d_click) {
                current_signal = SIGNAL_BTN_D_CLICK;
            } else if (btn.r_click) {
                current_signal = SIGNAL_BTN_R_CLICK;
            } else if (btn.l_long) {
                current_signal = SIGNAL_BTN_L_LONG;
            } else if (btn.u_long) {
                current_signal = SIGNAL_BTN_U_LONG;
            } else if (btn.d_long) {
                current_signal = SIGNAL_BTN_D_LONG;
            } else if (btn.r_long) {
                current_signal = SIGNAL_BTN_R_LONG;
            }
        }
        if (btn.value == 0x00) current_signal = SIGNAL_NONE;
        btn.value = 0x00;

        //  Основной автомат:
        top_level_sm(current_signal);
    }
}

/*-------------------------------------------- System initialization function */
void RCC_init(void)
{
    uint16_t tick;
    RCC->APB1ENR |= (RCC_APB1ENR_PWREN);
    PWR->CR = (PWR->CR & ~(PWR_CR_VOS)) | PWR_CR_VOS_0;
    RCC->CR |= RCC_CR_HSION;
    tick = 0;
    while (!(RCC->CR & RCC_CR_HSIRDY)) {
        if ((++tick) > TIMEOUT_VALUE) return;
    }
    RCC->CFGR |= RCC_CFGR_PLLSRC_HSI | RCC_CFGR_PLLMUL4 | RCC_CFGR_PLLDIV2;
    RCC->CR |= RCC_CR_PLLON;
    tick = 0;
    while (!(RCC->CR & RCC_CR_PLLRDY)) {
        if ((++tick) > TIMEOUT_VALUE) return;
    }
    FLASH->ACR |= FLASH_ACR_LATENCY;
    FLASH->ACR |= FLASH_ACR_PRFTEN;

    RCC->CFGR |= RCC_CFGR_SW_PLL;
    tick = 0;
    while (!(RCC->CFGR & RCC_CFGR_SWS_PLL)) {
        if ((++tick) > TIMEOUT_VALUE) return;
    }
}

void IWDG_init (void)
{
    RCC->CSR |= RCC_CSR_LSION;
    while(!(RCC->CSR & RCC_CSR_LSIRDY));

    IWDG->KR = IWDG_START;
    IWDG->KR = IWDG_WRITE_ACCESS;
    IWDG->PR = (IWDG_PR_PR_2 | IWDG_PR_PR_1 | IWDG_PR_PR_0);    //  divide to 256
    IWDG->RLR = IWDG_RELOAD;
    while(IWDG->SR);
    IWDG->KR = IWDG_REFRESH;
}

void SysTick_init(void)
{
    SysTick->LOAD = 31999;
    SysTick->VAL  = 31999;
    NVIC_SetPriority(SysTick_IRQn, 7);
    SysTick->CTRL =(SysTick_CTRL_CLKSOURCE_Msk | SysTick_CTRL_TICKINT_Msk | SysTick_CTRL_ENABLE_Msk);
}

void test_pin_init (void)
{
    /* (1) Enable the peripheral clock of GPIOA */
    /* (2) Select output mode (01) on GPIOA pin 7 */
    RCC->IOPENR |= RCC_IOPENR_GPIOAEN;                                              /* (1) */
    GPIOA->MODER = (GPIOA->MODER & ~(GPIO_MODER_MODE0)) | (GPIO_MODER_MODE0_0);     /* (2) */
}

/*-------------------------------------------------- System interrupt handler */

void NMI_Handler(void)
{
    while(1);
}

void SysTick_Handler(void)
{
    if (uptime.ms < 999) {
        uptime.ms++;
    } else {
        uptime.ms = 0;
        uptime.sec++;
    }

    if (flag.display_cnt < 124) {
        flag.display_cnt++;
    } else {
        flag.display_cnt = 0;
        display_upd_en();
    }

    if (flag.btn_cnt < 9) {
        flag.btn_cnt++;
    } else {
        flag.btn_cnt = 0;
        flag.btn_upd = 1;
    }

    GPIOA->ODR ^= GPIO_ODR_OD0;
}
