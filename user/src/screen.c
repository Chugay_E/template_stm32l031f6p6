/**
 * @file		screen.c
 * @author		Chugay E.A.
 * @version		0.01
 * @date		22.04.2020
 * @brief		Функции отрисовки экрана с использованием графических примитивов
 * File used UTF-8 code page. Don't convert other codepage for correct work.
 */

/*----------------------------------------------------------------- Includes: */
#include "screen.h"

/*-------------------------------------------------------------------- Macro: */
#define FONT_16_PIX     FONT_H16
#define FONT_08_PIX     FONT_NORMAL

/*--------------------------------------------------------- Extern variables: */

/*--------------------------------------------------------- Public variables: */

/*---------------------------------------------- Private funcrion prototypes: */

/*-------------------------------------------------------- Private variables: */
static const char *top_level_list[] = {
    [STR_1] = "STRING_1",
    [STR_2] = "STRING_2",
    [STR_3] = "STRING_3",
    [STR_4] = "STRING_4",
    [STR_5] = "STRING_5",
    [STR_6] = "STRING_6",
    [STR_7] = "STRING_7",
    [STR_8] = "STRING_8"
};

static const char *message_error[] = {
    "ОШИБКА",
    "/!\\",
    "ERROR"
};

static const char *message_warning[] = {
    "ВНИМАНИЕ",
    "/!\\",
    "WARNING"
};

/*----------------------------------------------------------------- Function: */
void top_level_screen (uint8_t signal)
{
    frame_font_select(FONT_16_PIX);
    widget_menu_list(top_level_list, (TOP_LEVEL_SELECT - 1), 16, signal);

    frame_font_select(FONT_08_PIX);
    widget_btn_l("EXIT", 0);
    widget_btn_up(0x1E, 0);
    widget_btn_down(0x1F, 0);
    widget_btn_r("ENTER", 0);
}



void msg_screen (uint8_t msg_id)
{
    frame_font_select(FONT_16_PIX);
    switch (msg_id) {
    case MSG_ERROR:
        widget_message(message_error, 3, 16);
        break;
    case MSG_WARNING:
        widget_message(message_warning, 3, 16);
        break;
    }

    frame_font_select(FONT_08_PIX);
    widget_btn_l("EXIT", 0);
//  widget_btn_m("[O]", (BTN_U | BTN_D));
}



/*----------------------------------------------------------------------------*/
/*--------------------------------------------------------- Private function: */
/*----------------------------------------------------------------------------*/
