/**
 * @file		ssd1306.с
 * @author		Chugay E.A.
 * @version		0.03 - Добавлена работа по интерфейсу I2C для серии L0.
 * @date		26.05.2019
 *
 * @brief       Функции, обеспечивающие инициализацию перефирийных модулей МК,
 *              инициализацию контроллера SSD1306 и их совместную работу.
 * @details
 * File used UTF-8 code page. Don't convert other codepage for correct work.
 */

/*------------------------------------------------------------------ Includes */
#include "ssd1306.h"

/*-------------------------------------------------------- External variables */
extern volatile uint8_t frame_buf[BUF_SIZE];

/*--------------------------------------------------------- Private variables */
static union {
    struct {
        volatile uint8_t display_upd_en  :1;    //  Флаг
        volatile uint8_t frame_buf_busy	 :1;	//  Флаг равен 1 при передаче кадра в дисплей
        volatile uint8_t frame_buf_ready :1;	//  Флаг должен быть установлен в 1 после окончания формирования кадра
        volatile uint8_t page_switch	 :5;	//  номер передаваемой страницы буфера
    };
    volatile uint8_t value;
} display_flag;

static const uint8_t init_array[] = {
    0xAE,   //	display OFF
    0xA8,   //	set MUX ratio
    0x3F,   //	MUX ratio 64
    0xD3,   //	set display offset
    0x00,   //	offset 0
    0x40,   //	set display start line 0
//  0xA1,   //	set segment remap (A0-no remap; A1-remap)               (шлейф вниз)
//  0xC8,   //	set COM output scan direction (C0-no remap; C8 remap)   (шлейф вниз)
    0xA0,   //	set segment remap (A0-no remap; A1-remap)               (шлейф вверх)
    0xC0,   //	set COM output scan direction (C0-no remap; C8 remap)   (шлейф вверх)
    0xDA,   //	set COM pins hardware config
    0x12,   //	A4 == 1; no left/right remap
    0x81,   //	set contrast control
    0xB4,   //	contrast == 180
    0xA4,   //	entire display ON
    0xA6,   //	set normal, no inverse
    0xD5,   //	set OSC freq.
    0x80,   //	set display clock divide ratio; OSC freq.
    0x8D,   //	enable charge pump regulator
    0x14,   //	0x10 external; 0x14 internal
    0x20,   //	set memory addressing mode
    0x00,   //	00-horizontal; 01-vertical; 10-page
    0x21,   //	set column address
    0x00,   //	start column == 0
    0x7F,   //	end column == 127
    0x22,   //	set page address
    0x00,   //	start page == 0
    0x07,   //	end page == 7
    0xAF,   //	display ON
};



/*----------------------------------------------- Private function prototypes */
#if defined (DISPLAY_SPI1) || defined (DISPLAY_SPI2)
/**
 * @brief	Инициализация SPI для работы с SSD1306
 * @param	Нет
 * @retval	Нет
 */
static void ssd1306_SPI_init (void);

/**
 * @brief	Отключение SPI, DMA, и перевод выводов в плавающий вход
 * @param	Нет
 * @retval	Нет
 */
static void ssd1306_SPI_deinit (void);
#elif defined (DISPLAY_I2C1)
/**
 * @brief	Инициализация I2C для работы с SSD1306
 * @param	Нет
 * @retval	Нет
 */
static void ssd1306_I2C_init(void);

/**
 * @brief	Отключение I2C, DMA, и перевод выводов в аналоговый вход
 * @param	Нет
 * @retval	Нет
 */
static void ssd1306_I2C_deinit (void);
#endif

/**
 * @brief	Стартовая инициализация контроллера дисплея
 * @param	Нет
 * @retval	Нет
 */
static void ssd1306_start_init (void);

/**
 * @brief	Настройка яркости дисплея (0..255)
 * @param	value - новое значение контрастности
 * @retval	Нет
 */
static void ssd1306_set_contrast (uint8_t value);

/**
 * @brief	Запуск передачи буфера в дисплей
 * @param	Нет
 * @retval	Нет
 */
static void ssd1306_DMA_send (void);

/**
 * @brief	Передача команды отключения дисплея
 * @param	Нет
 * @retval	Нет
 */
static void ssd1306_standby (void);

#if defined (DISPLAY_SPI1) || defined (DISPLAY_SPI2)
/**
 * @brief	Отправка одного байта в дисплей
 * @param	data - передаваемый байт
 * @retval	Нет
 */
static void ssd1306_send_byte (uint8_t data);
#endif



/*----------------------------------------------------------------------------*/
/*---------------------------------------------------------------- Functions: */
/*------------------------------------------------------------------ Функции: */
/*----------------------------------------------------------------------------*/

//  Инициализация дисплея
void init_display (void)
{
#if defined (DISPLAY_SPI1) || defined (DISPLAY_SPI2)
    ssd1306_SPI_init();
    ssd1306_start_init();
#elif defined (DISPLAY_I2C1)
    ssd1306_I2C_init();
    ssd1306_start_init();
#endif
}

//  Де-инициализация дисплея
void deinit_display (void)
{
    ssd1306_standby();
#if defined (DISPLAY_SPI1) || defined (DISPLAY_SPI2)
    ssd1306_SPI_deinit();
#elif defined (DISPLAY_I2C1)
    ssd1306_I2C_deinit();
#endif
}

//  Настройка контрастности дисплея
void display_set_contrast (uint8_t value)
{
    ssd1306_set_contrast(value);
}

//  передача кадра в дисплей
void send_frame_to_display (uint8_t signal, void(*frame)(uint8_t))
{
    if (!display_flag.frame_buf_busy) {
        frame_clear_buf();
        (*frame)(signal);
        display_flag.frame_buf_ready = 1;
    }
    ssd1306_DMA_send();
}

//  Установка флага, разрешающего обновление кадра
void display_upd_en (void)
{
    display_flag.display_upd_en = 1;
}

//  Запрос состояния флага обновления кадра
uint8_t display_upd_get (void)
{
    return display_flag.display_upd_en ? 1 : 0;
}



/*----------------------------------------------------------------------------*/
/*--------------------------------------------------------- Private function: */
/*------------------------------------------------------- Внутренние функции: */
/*----------------------------------------------------------------------------*/

#ifdef STM32F0_SERIES
#ifdef DISPLAY_SPI1
static void ssd1306_SPI_init(void)
{
    RCC->AHBENR |= (RCC_AHBENR_GPIOAEN | RCC_AHBENR_GPIOBEN);
    //	PB1 - RESET
    GPIOB->MODER   &= ~GPIO_MODER_MODER1;		//	input
    GPIOB->OTYPER  &= ~GPIO_OTYPER_OT_1;		//	push/pull
    GPIOB->PUPDR   &= ~GPIO_PUPDR_PUPDR1;		//	no pull-up or pull-down
    GPIOB->MODER   |= GPIO_MODER_MODER1_0;		//	output
    GPIOB->OSPEEDR |= GPIO_OSPEEDR_OSPEEDR1;	//	high speed
    //	PA6 - CHIP ENABLE
    GPIOA->MODER   &= ~GPIO_MODER_MODER6;		//	input
    GPIOA->OTYPER  &= ~GPIO_OTYPER_OT_6;		//	push/pull
    GPIOA->PUPDR   &= ~GPIO_PUPDR_PUPDR6;		//	no pull-up or pull-down
    GPIOA->MODER   |= GPIO_MODER_MODER6_0;		//	output
    GPIOA->OSPEEDR |= GPIO_OSPEEDR_OSPEEDR6;	//	high speed
    //	PA4 - SEND_DATA/SEND_CMD
    GPIOA->MODER   &= ~GPIO_MODER_MODER4;		//	input
    GPIOA->OTYPER  &= ~GPIO_OTYPER_OT_4;		//	push/pull
    GPIOA->PUPDR   &= ~GPIO_PUPDR_PUPDR4;		//	no pull-up or pull-down
    GPIOA->MODER   |= GPIO_MODER_MODER4_0;		//	output
    GPIOA->OSPEEDR |= GPIO_OSPEEDR_OSPEEDR4;	//	high speed
    //	PA7 - MOSI
    GPIOA->MODER  &= ~GPIO_MODER_MODER7;		//	input
    GPIOA->OTYPER &= ~GPIO_OTYPER_OT_7;			//	push/pull
    GPIOA->PUPDR  &= ~GPIO_PUPDR_PUPDR7;		//	no pull-up or pull-down
    GPIOA->MODER  |= GPIO_MODER_MODER7_1;		//	alternate func.
    GPIOA->AFR[0] &= ~GPIO_AFRL_AFSEL7;			//	SPI1 MOSI
    //	PA5 - SCK
    GPIOA->MODER  &= ~GPIO_MODER_MODER5;		//	input
    GPIOA->OTYPER &= ~GPIO_OTYPER_OT_5;			//	push/pull
    GPIOA->PUPDR  &= ~GPIO_PUPDR_PUPDR5;		//	no pull-up or pull-down
    GPIOA->MODER  |= GPIO_MODER_MODER5_1;		//	alternate func.
    GPIOA->AFR[0] &= ~GPIO_AFRL_AFSEL5;			//	SPI1 SCK
    //	CPOL=0, CPHA=0, 8 bit frame, 3.0 MHz CLK:
    RCC->APB2ENR |= RCC_APB2ENR_SPI1EN;			//	подать тактирование на модуль SPI2
    SPI1->CR1 = 0x0000;
    SPI1->CR2 = 0x0000;
    SPI1->CR1 |= (SPI_CR1_BIDIMODE | SPI_CR1_BIDIOE | SPI_CR1_MSTR |
                  SPI_CR1_BR_1 | SPI_CR1_BR_0 | SPI_CR1_SSI | SPI_CR1_SSM);	//	2-wire state
    SPI1->CR1 |= SPI_CR1_SPE;									//	включить SPI
#ifdef DMA_MODE
    //	настройка источника и приемника:
    RCC->AHBENR |= RCC_AHBENR_DMAEN;				//	тактирование DMA
    DMA1_Channel3->CPAR = (uint32_t)&SPI_DR;		//	адрес регистра перефирии
    DMA1_Channel3->CMAR = (uint32_t)&frame_buf[0];	//	массив данных в памяти
    DMA1_Channel3->CNDTR = BUF_SIZE;				//	количество байт для передачи
    //	инкрементация для перехода по элементам массива в памяти
    //	чтение из памяти, прерывание при передаче половины и окончании
    DMA1_Channel3->CCR = 0;
    DMA1_Channel3->CCR |= (DMA_CCR_MINC | DMA_CCR_DIR | DMA_CCR_TCIE);
    SPI1->CR2 |= SPI_CR2_TXDMAEN;					//	разрешить работу SPI2 с DMA
//	NVIC_SetPriority(DMA1_Channel2_3_IRQn, X);		//	приоритет прерывания
    NVIC_EnableIRQ(DMA1_Channel2_3_IRQn);			//	разрешить прерывания от DMA, канал 3
#endif	//	DMA MODE
}

//	отключение перефирии, обеспечивающей работу дисплея
static void ssd1306_SPI_deinit(void)
{
    NVIC_DisableIRQ (SPI1_IRQn);
    RCC->APB2ENR &= ~RCC_APB2ENR_SPI1EN;
#ifdef DMA_MODE
    NVIC_DisableIRQ (DMA1_Channel2_3_IRQn);
    RCC->AHBENR &= ~RCC_AHBENR_DMAEN;
#endif	//	DMA MODE
    GPIOB->MODER &= ~GPIO_MODER_MODER1;		//	input	PB1 - RESET
    GPIOA->MODER &= ~GPIO_MODER_MODER6;		//	input	PA6 - CHIP ENABLE
    GPIOA->MODER &= ~GPIO_MODER_MODER4;		//	input	PA4 - SEND_DATA/SEND_CMD
    GPIOA->MODER &= ~GPIO_MODER_MODER7;		//	input	PA7 - MOSI
    GPIOA->MODER &= ~GPIO_MODER_MODER5;		//	input	PA5 - SCK
}
#endif	//	DISPLAY_SPI1
#endif	//	STM32F0_SERIES

#ifdef DMA_MODE
#ifdef STM32F0_SERIES
#ifdef DISPLAY_SPI1
//	Запуск пересылки массива данных в дисплей
static void ssd1306_DMA_send(void)
{
    // передаем данные если модуль DMA был отключен и установлен флаг готовности буфера кадра:
    if (!(DMA1_Channel3->CCR & DMA_CCR_EN) && display_flag.frame_buf_ready) {
        SEND_DATA;								// режим передачи данных
        TX_ENABLE;
        DMA1_Channel3->CNDTR = BUF_SIZE;		// количество байт для передачи
        DMA1_Channel3->CCR |= DMA_CCR_EN;		// включение канала
        display_flag.frame_buf_busy = 1;
    } else {
        return;
    }
}
//	Обработчик прерывания по завершении передачи массива в дисплей
void DMA1_Channel2_3_IRQHandler(void)
{
    if (DMA1->ISR & DMA_ISR_TCIF3) {
        DMA1->IFCR |= DMA_IFCR_CTCIF3;			// сброс флага прерывания по окончании передачи от 3 канала DMA
        DMA1_Channel3->CCR &= ~DMA_CCR_EN;		// отключение канала
        while (!(SPI_SR & SPI_SR_TXE));
        while (SPI_SR & SPI_SR_BSY);			// ожидание конца передачи
        TX_DISABLE;								// отключаем прием данных дисплеем
        display_flag.value = 0x00;				// буфер готов к модификации
        return;
    } else {
        return;
    }
}
#endif	//	DISPLAY_SPI1
#endif	//	STM32F0_SERIES
#endif	//	DMA_MODE


#ifdef STM32F1_SERIES
#ifdef DISPLAY_SPI1
static void ssd1306_SPI_init (void)
{
    //	тактирование альтернативных функций, GPIOA и GPIOC
    RCC->APB2ENR |= (RCC_APB2ENR_IOPAEN | RCC_APB2ENR_IOPCEN | RCC_APB2ENR_AFIOEN);
    //	PA6 - RESET
    GPIOA->CRL &= ~(GPIO_CRL_CNF6);     //	GPIO push-pull
    GPIOA->CRL |= (GPIO_CRL_MODE6_0);	//	10 MHz
    //	PC5 - CHIP ENABLE
    GPIOC->CRL &= ~(GPIO_CRL_CNF5);     //	GPIO push-pull
    GPIOC->CRL |= (GPIO_CRL_MODE5_0);	//	10 MHz
    //	PA5 - SCK
    GPIOA->CRL |= GPIO_CRL_MODE5_0;     //	10 MHz
    GPIOA->CRL &= ~GPIO_CRL_CNF5;
    GPIOA->CRL |= GPIO_CRL_CNF5_1;		//	Alt. func
    //	PC4 - SEND_DATA/SEND_CMD
    GPIOC->CRL &= ~(GPIO_CRL_CNF4);     //	GPIO push-pull
    GPIOC->CRL |= (GPIO_CRL_MODE4_0);	//	10 MHz
    //	PA7 - MOSI
    GPIOA->CRL |= GPIO_CRL_MODE7_0;     //	10 MHz
    GPIOA->CRL &= ~GPIO_CRL_CNF7;
    GPIOA->CRL |= GPIO_CRL_CNF7_1;		//	Alt. func.
    //	CPOL=0, CPHA=0, 8 bit frame, 4.5 MHz CLK:
    RCC->APB2ENR |= RCC_APB2ENR_SPI1EN;	//	подать тактирование на модуль SPI1
    SPI1->CR1 = 0x0000;
    SPI1->CR2 = 0x0000;
    SPI1->CR1 |= (SPI_CR1_BIDIMODE | SPI_CR1_BIDIOE | SPI_CR1_MSTR |
                  SPI_CR1_BR_1 | SPI_CR1_BR_0 | SPI_CR1_SSI | SPI_CR1_SSM);	//	2-wire state
    SPI1->CR1 |= SPI_CR1_SPE;			//	включить SPI
#ifdef DMA_MODE
    //	настройка источника и приемника:
    RCC->AHBENR |= RCC_AHBENR_DMA1EN;				//	тактирование DMA1
    DMA1_Channel3->CPAR = (uint32_t)&SPI_DR;		//	адрес регистра перефирии
    DMA1_Channel3->CMAR = (uint32_t)&frame_buf[0];	//	массив данных в памяти
    DMA1_Channel3->CNDTR = SCREEN_W;				//	количество байт для передачи (одна строка)
    //	инкрементация для перехода по элементам массива в памяти
    //	чтение из памяти, прерывание при передаче половины и окончании
    DMA1_Channel3->CCR = 0;
    DMA1_Channel3->CCR |= (DMA_CCR3_MINC | DMA_CCR3_DIR | DMA_CCR3_TCIE);
    SPI1->CR2 |= SPI_CR2_TXDMAEN;					//	разрешить работу SPI1 с DMA
//	NVIC_SetPriority(DMA1_Channel3_IRQn, X);		//	приоритет прерывания
    NVIC_EnableIRQ (DMA1_Channel3_IRQn);			//	разрешить прерывания от DMA, канал 3
#endif	//	DMA MODE
}

//	отключение перефирии, обеспечивающей работу дисплея
static void ssd1306_SPI_deinit (void)
{
    //	отключение тактирования переферии:
    NVIC_DisableIRQ (SPI1_IRQn);
    RCC->APB2ENR &= (uint32_t)~RCC_APB2ENR_SPI1EN;
#ifdef DMA_MODE
    NVIC_DisableIRQ (DMA1_Channel3_IRQn);
    RCC->AHBENR &= (uint32_t)~RCC_AHBENR_DMA1EN;
#endif	//	DMA MODE
    //	перевод всех выводов в состояние входа:
    //	PA6 - RESET
    GPIOA->CRL &= ~(GPIO_CRL_CNF6 | GPIO_CRL_MODE6);
    GPIOA->CRL |= (GPIO_CRL_CNF6_0);
    //	PC5 - CHIP ENABLE
    GPIOC->CRL &= ~(GPIO_CRL_CNF5 | GPIO_CRL_MODE5);
    GPIOC->CRL |= (GPIO_CRL_CNF5_0);
    //	PA5 - SCK
    GPIOA->CRL &= ~(GPIO_CRL_CNF5 | GPIO_CRL_MODE5);
    GPIOA->CRL |= (GPIO_CRL_CNF5_0);
    //	PC4 - SEND_DATA/SEND_CMD
    GPIOC->CRL &= ~(GPIO_CRL_CNF4 | GPIO_CRL_MODE4);
    GPIOC->CRL |= (GPIO_CRL_CNF4_0);
    //	PA7 - MOSI
    GPIOA->CRL &= ~(GPIO_CRL_CNF7 | GPIO_CRL_MODE7);
    GPIOA->CRL |= (GPIO_CRL_CNF7_0);
}
#endif	//	DISPLAY_SPI1

#ifdef DISPLAY_SPI2
void ssd1306_SPI_init(void)
{
    //	тактирование альтернативных функций и GPIOB
    RCC->APB2ENR |= (RCC_APB2ENR_IOPBEN | RCC_APB2ENR_AFIOEN);
    //	PB11 - RESET
    GPIOB->CRH &= ~(GPIO_CRH_CNF11);	//	GPIO push-pull
    GPIOB->CRH |= (GPIO_CRH_MODE11_0);	//	10 MHz
    //	PB12 - CHIP ENABLE
    GPIOB->CRH &= ~(GPIO_CRH_CNF12);	//	GPIO push-pull
    GPIOB->CRH |= (GPIO_CRH_MODE12_0);	//	10 MHz
    //	PB13 - SCK
    GPIOB->CRH |= GPIO_CRH_MODE13_0;	//	10 MHz
    GPIOB->CRH &= ~GPIO_CRH_CNF13;
    GPIOB->CRH |= GPIO_CRH_CNF13_1;		//	Alt. func
    //	PB14 - SEND_DATA/SEND_CMD
    GPIOB->CRH &= ~(GPIO_CRH_CNF14);	//	GPIO push-pull
    GPIOB->CRH |= (GPIO_CRH_MODE14_0);	//	10 MHz
    //	PB15 - MOSI
    GPIOB->CRH |= GPIO_CRH_MODE15_0;	//	10 MHz
    GPIOB->CRH &= ~GPIO_CRH_CNF15;
    GPIOB->CRH |= GPIO_CRH_CNF15_1;		//	Alt. func.
    //	CPOL=0, CPHA=0, 8 bit frame, 3.0 MHz CLK:
    RCC->APB1ENR |= RCC_APB1ENR_SPI2EN;	//	подать тактирование на модуль SPI2
    SPI2->CR1 = 0x0000;
    SPI2->CR2 = 0x0000;
    SPI2->CR1 |= (SPI_CR1_BIDIMODE | SPI_CR1_BIDIOE | SPI_CR1_MSTR |
                  SPI_CR1_BR_1 | SPI_CR1_SSI | SPI_CR1_SSM);	//	2-wire state
    SPI2->CR1 |= SPI_CR1_SPE;			//	включить SPI
#ifdef DMA_MODE
    //	настройка источника и приемника:
    RCC->AHBENR |= RCC_AHBENR_DMA1EN;				//	тактирование DMA1
    DMA1_Channel5->CPAR = (uint32_t)&SPI_DR;		//	адрес регистра перефирии
    DMA1_Channel5->CMAR = (uint32_t)&frame_buf[0];	//	массив данных в памяти
    DMA1_Channel5->CNDTR = BUF_SIZE;				//	количество байт для передачи
    //	инкрементация для перехода по элементам массива в памяти
    //	чтение из памяти, прерывание при передаче половины и окончании
    DMA1_Channel5->CCR = 0;
    DMA1_Channel5->CCR |= (DMA_CCR5_MINC | DMA_CCR5_DIR | DMA_CCR5_TCIE);
    SPI2->CR2 |= SPI_CR2_TXDMAEN;					//	разрешить работу SPI2 с DMA
//	NVIC_SetPriority(DMA1_Channel5_IRQn, X);		//	приоритет прерывания
    NVIC_EnableIRQ (DMA1_Channel5_IRQn);			//	разрешить прерывания от DMA, канал 5
#endif	//	DMA MODE
}

//	отключение перефирии, обеспечивающей работу дисплея
static void ssd1306_SPI_deinit(void)
{
    //	отключение тактирования переферии:
    NVIC_DisableIRQ (SPI2_IRQn);
    RCC->APB1ENR &= ~RCC_APB1ENR_SPI2EN;
#ifdef DMA_MODE
    NVIC_DisableIRQ (DMA1_Channel5_IRQn);
    RCC->AHBENR &= ~RCC_AHBENR_DMA1EN;
#endif	//	DMA MODE
    //	перевод всех выводов в состояние входа:
    //	PB11 - RESET
    GPIOB->CRH &= ~(GPIO_CRH_CNF11 | GPIO_CRH_MODE11);
    GPIOB->CRH |= (GPIO_CRH_CNF11_0);
    //	PB12 - CHIP ENABLE
    GPIOB->CRH &= ~(GPIO_CRH_CNF12 | GPIO_CRH_MODE12);
    GPIOB->CRH |= (GPIO_CRH_CNF12_0);
    //	PB13 - SCK
    GPIOB->CRH &= ~(GPIO_CRH_CNF13 | GPIO_CRH_MODE13);
    GPIOB->CRH |= (GPIO_CRH_CNF13_0);
    //	PB14 - SEND_DATA/SEND_CMD
    GPIOB->CRH &= ~(GPIO_CRH_CNF14 | GPIO_CRH_MODE14);
    GPIOB->CRH |= (GPIO_CRH_CNF14_0);
    //	PB15 - MOSI
    GPIOB->CRH &= ~(GPIO_CRH_CNF15 | GPIO_CRH_MODE15);
    GPIOB->CRH |= (GPIO_CRH_CNF15_0);
}
#endif	//	DISPLAY_SPI2
#endif	//	STM32F1_SERIES


#ifdef STM32L0_SERIES
#ifdef DISPLAY_I2C1
static void ssd1306_I2C_init(void)
{
    //  PA9 - SCL; PA10 - SDA:
    RCC->IOPENR |= RCC_IOPENR_GPIOAEN;
    GPIOA->PUPDR &= ~(GPIO_PUPDR_PUPD9   | GPIO_PUPDR_PUPD10);
    GPIOA->PUPDR  |= (GPIO_PUPDR_PUPD9_0 | GPIO_PUPDR_PUPD10_0);
    GPIOA->OTYPER |= (GPIO_OTYPER_OT_9   | GPIO_OTYPER_OT_10);
    GPIOA->AFR[1] &= (GPIO_AFRH_AFSEL9   | GPIO_AFRH_AFSEL10);
    GPIOA->AFR[1] |= (0x00000110);
    GPIOA->MODER &= ~(GPIO_MODER_MODE9   | GPIO_MODER_MODE10);
    GPIOA->MODER |=  (GPIO_MODER_MODE9_1 | GPIO_MODER_MODE10_1);

    //  I2C1 init:
    RCC->APB1ENR |=  RCC_APB1ENR_I2C1EN;
    RCC->CCIPR   &= ~RCC_CCIPR_I2C1SEL;
    I2C1->TIMINGR = (uint32_t)0x20302E37;
    I2C1->CR1 &= ~(I2C_CR1_TXIE | I2C_CR1_RXIE);
    I2C1->CR1 |= I2C_CR1_PE;
    I2C1->CR2 |= I2C_CR2_AUTOEND | (DISPLAY_ADDR << 1);
    NVIC_SetPriority(I2C1_IRQn, 15);    //  Раскомментировать, если на шине
    NVIC_EnableIRQ(I2C1_IRQn);          //  есть другие устройства.

#ifdef DMA_MODE
    //  DMA channel 2:
    RCC->AHBENR |= RCC_AHBENR_DMA1EN;
    DMA1_CSELR->CSELR = (DMA1_CSELR->CSELR & ~DMA_CSELR_C2S) | (6 << 4);
    DMA1_Channel2->CPAR = (uint32_t)&(I2C1->TXDR);
    DMA1_Channel2->CCR |= DMA_CCR_MINC | DMA_CCR_DIR | DMA_CCR_TCIE;
    I2C1->CR1 |= I2C_CR1_TXDMAEN;
    NVIC_SetPriority(DMA1_Channel2_3_IRQn, 14);
    NVIC_EnableIRQ(DMA1_Channel2_3_IRQn);
#endif  //  DMA_MODE
}

//	отключение перефирии, обеспечивающей работу дисплея
static void ssd1306_I2C_deinit(void)
{
    //	отключение тактирования переферии:
    RCC->APB1ENR &= ~RCC_APB1ENR_I2C1EN;
#ifdef DMA_MODE
    NVIC_DisableIRQ (DMA1_Channel2_3_IRQn);
    RCC->AHBENR &= ~RCC_AHBENR_DMA1EN;
#endif	//	DMA MODE
    //	перевод всех выводов в состояние аналогового входа:
    //	PA9 - SCL; PA10 - SDA:
    GPIOA->MODER  &= ~(GPIO_MODER_MODE9 | GPIO_MODER_MODE10);
    GPIOA->OTYPER &= ~(GPIO_OTYPER_OT_9 | GPIO_OTYPER_OT_10);
}
#endif  //  DISPLAY_I2C1
#endif  //  STM32L0_SERIES

#ifdef DMA_MODE
#ifdef STM32F1_SERIES
#ifdef DISPLAY_SPI1
//	Запуск пересылки массива данных в дисплей
static void ssd1306_DMA_send (void)
{
    // передаем данные если модуль DMA был отключен и установлен флаг готовности буфера кадра:
    if (!(DMA1_Channel3->CCR & DMA_CCR3_EN) && display_flag.frame_buf_ready) {
        SEND_DATA;								// режим передачи данных
        TX_ENABLE;
        DMA1_Channel3->CNDTR = BUF_SIZE;        // количество байт для передачи
        DMA1_Channel3->CCR |= DMA_CCR3_EN;      // включение канала
        display_flag.frame_buf_busy = 1;
    } else {
        return;
    }
}
//	Обработчик прерывания по завершении передачи массива в дисплей
void DMA1_Channel3_IRQHandler (void)
{
    //	Обработка прерывания по завершению передачи DMA1_Channel3 --> SPI1:
    if (DMA1->ISR & DMA_ISR_TCIF3) {
        DMA1->IFCR |= DMA_IFCR_CTCIF3;          // сброс флага прерывания по окончании передачи от 3 канала DMA
        DMA1_Channel3->CCR &= ~DMA_CCR3_EN;     // отключение канала
        while (!(SPI_SR & SPI_SR_TXE));
        while (SPI_SR & SPI_SR_BSY);            // ожидание конца передачи
        TX_DISABLE;                             // отключаем прием данных дисплеем
        display_flag.value = 0x00;				// буфер готов к модификации
        return;
    }
}
#endif	//	DISPLAY_SPI1

#ifdef DISPLAY_SPI2
//	Запуск пересылки массива данных в дисплей
static void ssd1306_DMA_send(void)
{
    // передаем данные если модуль DMA был отключен и установлен флаг готовности буфера кадра:
    if (!(DMA1_Channel5->CCR & DMA_CCR5_EN) && display_flag.frame_buf_ready) {
        SEND_DATA;								// режим передачи данных
        TX_ENABLE;
        DMA1_Channel5->CNDTR = BUF_SIZE;		// количество байт для передачи
        DMA1_Channel5->CCR |= DMA_CCR5_EN;		// включение канала
        display_flag.frame_buf_busy = 1;
    } else {
        return;
    }
}
//	Обработчик прерывания по завершении передачи массива в дисплей
void DMA1_Channel5_IRQHandler(void)
{
    //	Обработка прерывания по завершению передачи DMA1_Channel5 --> SPI2:
    if (DMA1->ISR & DMA_ISR_TCIF5) {
        DMA1->IFCR |= DMA_IFCR_CTCIF5;			// сброс флага прерывания по окончании передачи от 5 канала DMA
        DMA1_Channel5->CCR &= ~DMA_CCR5_EN;		// отключение канала
        while (!(SPI_SR & SPI_SR_TXE));
        while (SPI_SR & SPI_SR_BSY);			// ожидание конца передачи
        TX_DISABLE;								// отключаем прием данных дисплеем
        display_flag.value = 0x00;				// буфер готов к модификации
        return;
    }
}
#endif	//	DISPLAY_SPI2
#endif	//	STM32F1_SERIES

#ifdef STM32L0_SERIES
#ifdef DISPLAY_I2C1
//	Запуск пересылки массива данных в дисплей
static void ssd1306_DMA_send(void)
{
    // передаем данные если модуль DMA был отключен и установлен флаг готовности буфера кадра:
    if (!(DMA1_Channel2->CCR & DMA_CCR_EN) && display_flag.frame_buf_ready) {
        while (DISPLAY_I2C->ISR & I2C_ISR_BUSY);        // ждем, если еще передается предыдущий пакет
        DISPLAY_I2C->CR1 &= ~(I2C_CR1_TXIE | I2C_CR1_RXIE);
        DISPLAY_I2C->CR2 &= ~((0xFF << 16) | (0x7F << 1) | I2C_CR2_RD_WRN);
        DISPLAY_I2C->CR2 |= (((SCREEN_W >> 1) + 1) << 16) | (DISPLAY_ADDR << 1);

        //  передача кадра пакетами: [байт адреса]-[байт признака данных]-[64 байта данных]
        static uint8_t tx_data[(SCREEN_W >> 1) + 1];
        tx_data[0] = DISPLAY_DATA;
        for (uint8_t i = 0; i < (SCREEN_W >> 1); i++) {
            tx_data[i + 1] = frame_buf[((SCREEN_W >> 1) * display_flag.page_switch) + i];
        }
        DMA1_Channel2->CMAR = (uint32_t)&tx_data[0];
        DMA1_Channel2->CNDTR = ((SCREEN_W >> 1) + 1);   // количество байт для передачи
        DMA1_Channel2->CCR |= DMA_CCR_EN;               // включение канала
        DISPLAY_I2C->CR2 |= I2C_CR2_START;              // Запускаем передачу

        display_flag.frame_buf_busy = 1;
        if (display_flag.page_switch >= ((SCREEN_H >> 2) - 1)) {
            display_flag.display_upd_en = 0;
        }
    } else {
        return;
    }
}
//	Обработчик прерывания по завершении передачи массива в дисплей
void DMA1_Channel2_3_IRQHandler(void)
{
    //	Обработка прерывания по завершению передачи DMA1_Channel2 --> I2C1:
    if (DMA1->ISR & DMA_ISR_TCIF2) {
        DMA1->IFCR |= DMA_IFCR_CTCIF2;
        DMA1_Channel2->CCR &= ~DMA_CCR_EN;
        display_flag.page_switch++;
        if (display_flag.page_switch >= (SCREEN_H >> 2)) {
            display_flag.value = 0;
        }
        return;
    }
}
#endif	//	DISPLAY_I2C1
#endif	//	STM32L0_SERIES
#endif	//	DMA_MODE



//	Отправка одного байта
#if defined (DISPLAY_SPI1) || defined (DISPLAY_SPI2)
static void ssd1306_send_byte(uint8_t data)
{
    SPI_DR = data;
    while(!(SPI_SR & SPI_SR_TXE));
}
#endif

//	Инициализация дисплея
static void ssd1306_start_init(void)
{
    display_flag.value = 0;				//	сброс всех флагов и счетчика
#if defined (DISPLAY_SPI1) || defined (DISPLAY_SPI2)
    RESET_ASSERT;						//	прижимаем линию сброса к земле, сброс контроллера дисплея
    TX_DISABLE;							//	отключаем прием дисплея
    SEND_CMD;							//	настраиваем передачу команды
    for (uint8_t i = 0; i < 100; i++);	//	не менее 3 мкс в состоянии сброса
    RESET_DEASSERT;						//	подаем питание на линию сброса
    for (uint8_t i = 0; i < 100; i++);	//	не менее 3 мкс
    TX_ENABLE;							//	включаем передачу
    for (uint8_t i = 0; i < 27; i++) ssd1306_send_byte(init_array[i]);
    while (SPI_SR & SPI_SR_BSY);
    TX_DISABLE;
#elif defined (DISPLAY_I2C1)
    while (DISPLAY_I2C->ISR & I2C_ISR_BUSY);
    DISPLAY_I2C->CR2 &= ~((0xFF << 16) | (0x7F << 1) | I2C_CR2_RD_WRN);
    DISPLAY_I2C->CR2 |= (2 << 16) | (DISPLAY_ADDR << 1);
    for (uint8_t i = 0; i < 27; i++) {
        DISPLAY_I2C->TXDR = DISPLAY_CMD;
        DISPLAY_I2C->CR2 |= I2C_CR2_START;
        while (!(DISPLAY_I2C->ISR & I2C_ISR_TXE));
        DISPLAY_I2C->TXDR = init_array[i];
        while (DISPLAY_I2C->ISR & I2C_ISR_BUSY);
    }
#endif
}

//	Настройка яркости
static void ssd1306_set_contrast(uint8_t value)
{
#ifdef DMA_MODE
    if (display_flag.value) return;
#endif
#if defined (DISPLAY_SPI1) || defined (DISPLAY_SPI2)
    SEND_CMD;
    TX_ENABLE;
    ssd1306_send_byte(0x81);	//	set contrast control
    ssd1306_send_byte(value);	//	contrast value
    while (SPI_SR & SPI_SR_BSY);
    TX_DISABLE;
#elif defined (DISPLAY_I2C1)
    while (DISPLAY_I2C->ISR & I2C_ISR_BUSY);
    DISPLAY_I2C->CR2 &= ~((0xFF << 16) | (0x7F << 1) | I2C_CR2_RD_WRN);
    DISPLAY_I2C->CR2 |= (2 << 16) | (DISPLAY_ADDR << 1);
    DISPLAY_I2C->TXDR = DISPLAY_CMD;
    DISPLAY_I2C->CR2 |= I2C_CR2_START;
    while (!(DISPLAY_I2C->ISR & I2C_ISR_TXE));
    DISPLAY_I2C->TXDR = 0x81;
    while (DISPLAY_I2C->ISR & I2C_ISR_BUSY);
    DISPLAY_I2C->TXDR = DISPLAY_CMD;
    DISPLAY_I2C->CR2 |= I2C_CR2_START;
    while (!(DISPLAY_I2C->ISR & I2C_ISR_TXE));
    DISPLAY_I2C->TXDR = value;
    while (DISPLAY_I2C->ISR & I2C_ISR_BUSY);
#endif
}

//	перевод дисплея в режим сна
static void ssd1306_standby(void)
{
    display_flag.value = 0;				//	сброс всех флагов и счетчика
#if defined (DISPLAY_SPI1) || defined (DISPLAY_SPI2)
    while (SPI_SR & SPI_SR_BSY);        //	ждем окончания передачи
    TX_DISABLE;
    SEND_CMD;
    TX_ENABLE;
    ssd1306_send_byte(0xAE);            //	display OFF
    TX_DISABLE;
    RESET_ASSERT;
#elif defined (DISPLAY_I2C1)
    while (DISPLAY_I2C->ISR & I2C_ISR_BUSY);
    DISPLAY_I2C->CR2 &= ~((0xFF << 16) | (0x7F << 1) | I2C_CR2_RD_WRN);
    DISPLAY_I2C->CR2 |= ((2 << 16) | (DISPLAY_ADDR << 1));
    DISPLAY_I2C->TXDR = DISPLAY_CMD;
    DISPLAY_I2C->CR2 |= I2C_CR2_START;
    while (!(DISPLAY_I2C->ISR & I2C_ISR_TXE));
    DISPLAY_I2C->TXDR = 0xAE;           //	display OFF
    while (DISPLAY_I2C->ISR & I2C_ISR_BUSY);
#endif
}
