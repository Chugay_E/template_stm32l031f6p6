/**
 * @file        display_config.h
 * @author      Chugay E.A.
 * @version     0.01
 *              0.02 - Добавлена работа с интерфейсом I2C для серии L0.
 * @date        06.02.2019
 * @brief       Макроопределения для условной компиляции файла "frame_builder.c"
 * @details
 * @n File used UTF-8 code page. Don't convert other codepage for correct work.
 * @n Файл использует конировку UTF-8 для корректного преобразования шрифтов
 * @n SCREEN_W - ширина экрана в пикселях.
 * @n SCREEN_H - высота экрана в пикселях.
 * @code
 *  (SCREEN_H - 1)  +-----------------------+                 BYTE: LSB
 *              ^   | string (0)            |                       ...
 *              ^   | ...                   |                       ...
 *    Y dot     ^   | ...                   |                       ...
 *  coordinate  ^   | ...                   |                       ...
 *              ^   | ...                   |                       ...
 *              ^   | string (MAX_STR_POS)  |                       ...
 *              0   +-----------------------+                       MSB
 *                  0   >   >   >   >   >   (SCREEN_W - 1)
 *                      X dot coordinate
 * @endcode
 * - Ввести размер размер дисплея в пикселях.
 * - Раскомментировать используемые шрифты.
 * - Раскомментировать используемую серию МК, или режим эмуляции консольным приложением.
 * - Раскомментировать определение с используемым интерфейсом.
 * - При использовании DMA раскомментировать соотв. строку.
 * - При необходимости скорректировать управление выводами под свой вариант подключения.
 * - При необходимости исправить функции инициализации/деинициализации выводов.
 */

#ifndef DISPLAY_CONFIG_H_
#define DISPLAY_CONFIG_H_

/*--------------------------------------------- Display dimension, in pixels: */
/*------------------------------------------------ Размер дисплея в пикселях: */
#define	SCREEN_W    (uint8_t)128    ///< Ширина экрана в пикселях
#define	SCREEN_H    (uint8_t)64     ///< Высота экрана в пикселях


/*-------------------------------------- Font selection, uncomment use fonts: */
/*------------------------------------ Раскомментировать используемые шрифты: */
    #define FONT_NORMAL     0       ///< "Простой" шрифт, высота символа не более 8 пикселей
//  #define FONT_NARROW     1       ///< "Узкий" шрифт, высота символа не более 8 пикселей
//  #define FONT_BOLD       2       ///< "Жирный" шрифт, высота символа не более 8 пикселей
//  #define FONT_HANDWRITE  3       ///< "Рукописный" шрифт, высота символа не более 8 пикселей
    #define FONT_H16        4       ///< "16-пиксельный" шрифт, высота символа не более 16 пикселей
//  #define FONT_H32        5       ///< "32-пиксельный" шрифт, высота символа не более 32 пикселей


/*---------------------------------- MCU series selection, uncomment use MPU: */
/*----------------------------------------------------------- Выбор серии МК: */
    #define STM32L0_SERIES
//  #define STM32F0_SERIES
//  #define STM32F1_SERIES
//  #define CONSOLE_EMULATOR


/*-------------------------------------------------- Uncomment use interface: */
/*--------------------------------- Раскомментировать используемый интерфейс: */
//  #define DISPLAY_SPI1
//  #define DISPLAY_SPI2
    #define DISPLAY_I2C1


/*---------------------------------------------------- Uncomment for use DMA: */
/*--------------------------------- Раскомментировать, если используется DMA: */
    #define DMA_MODE


/*-------------------------------------------------------- L0 series defines: */
#ifdef STM32L0_SERIES
    #include "stm32l0xx.h"
/*------------------------------------------------------------- I2C1 defines: */
    #ifdef DISPLAY_I2C1
        #define DISPLAY_I2C         I2C1            //  Интерфейс, к которому подключен дисплей
        #define DISPLAY_ADDR        (uint8_t)0x3C   //  Адрес дисплея на шине
        #define DISPLAY_CMD         (uint8_t)0x80   //  Следующий байт будет командой
        #define DISPLAY_DATA        (uint8_t)0x40   //  Следующие несколько байт будут данными
    #endif  //  I2C1 connection
#endif      //  series L0


/*-------------------------------------------------------- F0 series defines: */
#ifdef STM32F0_SERIES
    #include "stm32f0xx.h"
/*------------------------------------------------------------- SPI1 defines: */
    #ifdef DISPLAY_SPI1
        #define RESET_DEASSERT      GPIOB->BSRR = GPIO_BSRR_BS_1    //  PB1 reset on power line
        #define RESET_ASSERT        GPIOB->BSRR = GPIO_BSRR_BR_1    //  PB1 reset on GND line
        #define TX_DISABLE          GPIOA->BSRR = GPIO_BSRR_BS_6    //  PA6 CE on power line
        #define TX_ENABLE           GPIOA->BSRR = GPIO_BSRR_BR_6    //  PA6 CE on GND line
        #define SEND_DATA           GPIOA->BSRR = GPIO_BSRR_BS_4    //  PA4 DC on power line
        #define SEND_CMD            GPIOA->BSRR = GPIO_BSRR_BR_4    //  PA4 DC on GND line
        #define SPI_DR  (*(__IO uint8_t *)((uint32_t)&SPI1->DR))    //  SEND_DATA REGISTER
        #define SPI_SR              SPI1->SR                        //  STATUS REGISTER
    #endif  //  SPI1 connection
#endif      //  series F0


/*-------------------------------------------------------- F1 series defines: */
#ifdef STM32F1_SERIES
    #include "stm32f1xx.h"
/*------------------------------------------------------------- SPI1 defines: */
    #ifdef DISPLAY_SPI1
        #define RESET_DEASSERT      GPIOA->BSRR = GPIO_BSRR_BS6     //  PA6 reset on power line
        #define RESET_ASSERT        GPIOA->BSRR = GPIO_BSRR_BR6     //  PA6 reset on GND line
        #define TX_DISABLE          GPIOB->BSRR = GPIO_BSRR_BS0     //  PB0 CE on power line
        #define TX_ENABLE           GPIOB->BSRR = GPIO_BSRR_BR0     //  PB0 CE on GND line
        #define SEND_DATA           GPIOA->BSRR = GPIO_BSRR_BS4     //  PA4 DC on power line
        #define SEND_CMD            GPIOA->BSRR = GPIO_BSRR_BR4     //  PA4 DC on GND line
        #define SPI_DR              SPI1->DR                        //  SEND_DATA REGISTER
        #define SPI_SR              SPI1->SR                        //  STATUS REGISTER
    #endif  //  SPI1 connection
/*------------------------------------------------------------- SPI2 defines: */
    #ifdef DISPLAY_SPI2
        #define RESET_DEASSERT      GPIOB->BSRR = GPIO_BSRR_BS11    //  PB11 reset on power line
        #define RESET_ASSERT        GPIOB->BSRR = GPIO_BSRR_BR11    //  PB11 reset on GND line
        #define TX_DISABLE          GPIOB->BSRR = GPIO_BSRR_BS12    //  PB12 CE on power line
        #define TX_ENABLE           GPIOB->BSRR = GPIO_BSRR_BR12    //  PB12 CE on GND line
        #define SEND_DATA           GPIOB->BSRR = GPIO_BSRR_BS14    //  PB14 DC on power line
        #define SEND_CMD            GPIOB->BSRR = GPIO_BSRR_BR14    //  PB14 DC on GND line
        #define SPI_DR              SPI2->DR                        //  SEND_DATA REGISTER
        #define SPI_SR              SPI2->SR                        //  STATUS REGISTER
    #endif  //  SPI2 connection
#endif      //  series F1


#endif  //  DISPLAY_CONFIG_H_
